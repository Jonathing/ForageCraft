package me.jonathing.minecraft.foragecraft;

import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.forgespi.language.IModInfo;

/**
 * Contains various important pieces of information about the instance of ForageCraft. Originally based on constant
 * injection in ModUtil by Shadew.
 *
 * @author Jonathing
 * @since 2.0.0
 */
public final class ForageInfo
{
    private static final IModInfo CONTAINER = ModList.get().getModContainerById("foragecraft").get().getModInfo();

    private ForageInfo()
    {
    }

    /**
     * The Mod ID of ForageCraft, which is fixed to {@code foragecraft}.
     */
    public static final String MOD_ID;

    /**
     * The Mod Name of ForageCraft, which is fixed to 'ForageCraft'.
     */
    public static final String NAME;

    /**
     * This constant is true when the system property {@code foragecraft.iside} is {@code "true"}. This property is set
     * in all run configurations gradle.
     */
    public static final boolean IDE;

    /**
     * The version of the ForageCraft (ex. {@code 2.0.0}), which is dynamically injected on build. Defaults to {@code
     * NOT.A.VERSION}.
     */
    public static final String VERSION;

    /**
     * The version name of ForageCraft (ex. 'ForageCraft: Reborn'), which is dynamically injected on build.
     * Defaults to 'Not A Version'.
     */
    public static final String VERSION_NAME;

    static
    {
        MOD_ID = "foragecraft";

        NAME = Lazy.of(() ->
                (String) CONTAINER.getConfig().getConfigElement("displayName").get()
        ).get();

        VERSION = Lazy.of(
                CONTAINER.getVersion()::toString
        ).get();

        VERSION_NAME = Lazy.of(() ->
                (String) CONTAINER.getConfig().getConfigElement("versionName").get()
        ).get();

        IDE = Lazy.of(() ->
        {
            String p = System.getProperty("foragecraft.iside");
            return Boolean.parseBoolean(p);
        }).get();
    }
}
