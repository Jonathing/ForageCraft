package me.jonathing.minecraft.foragecraft.coremod.hooks;

import com.google.gson.JsonElement;
import me.jonathing.minecraft.foragecraft.data.ForageCraftData;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraftforge.fml.ModList;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.util.Map;

import static me.jonathing.minecraft.foragecraft.ForageCraft.LOGGER;

/**
 * This class contains hooks used in the {@link net.minecraft.world.item.crafting.RecipeManager RecipeManager} bytecode.
 *
 * @author Jonathing
 * @see net.minecraft.world.item.crafting.RecipeManager#apply(Map, ResourceManager, ProfilerFiller)
 * RecipeManager.apply(Map, ResourceManager, ProfilerFiller)
 * @since 2.2.2
 */
public final class RecipeManagerHooks
{
    private static final Marker MARKER = MarkerManager.getMarker("RecipeManagerCoreMod");

    private RecipeManagerHooks()
    {
    }

    /**
     * This modifies a given {@link Map} of {@link ResourceLocation}s and {@link JsonElement}s containing all of the
     * recipes to be parsed on world load. It uses the {@link net.minecraftforge.fml.ModList ModList} to check if a
     * particular mod is <em>not</em> loaded. This way, I can disable recipes specific for that mod so the console
     * doesn't shit itself when it tries to parse through that recipe.
     *
     * @param recipeMap The map of recipes to be loaded on world load.
     */
    public static void removeOptionalRecipes(Map<ResourceLocation, JsonElement> recipeMap)
    {
        ForageCraftData.OPTIONAL_RECIPES.forEach((modId, recipes) ->
        {
            if (!ModList.get().isLoaded(modId))
            {
                int size = recipes.size();
                LOGGER.debug(MARKER, "Skipping {} recipe{} since {} is not installed.", size, size == 1 ? "" : "s", modId);
                ForageCraftData.OPTIONAL_RECIPES.get(modId).forEach(recipeMap::remove);
            }
        });
    }
}
