package me.jonathing.minecraft.foragecraft.coremod.mixin;

import com.google.gson.JsonElement;
import me.jonathing.minecraft.foragecraft.coremod.hooks.RecipeManagerHooks;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.item.crafting.RecipeManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

/**
 * This mixin class is used to make certain modifications to the {@link RecipeManager} class to ForageCraft's needs.
 *
 * @author Jonathing
 * @see RecipeManager
 * @since 2.1.3
 */
@Mixin(RecipeManager.class)
public class RecipeManagerMixin
{
    /**
     * This method hooks into the {@link org.spongepowered.asm.mixin.injection.points.MethodHead MethodHead} of the
     * {@link RecipeManager#apply(Map, ResourceManager, ProfilerFiller)} method so it can give the map containing all of
     * the recipes to be loaded at world load to the {@link RecipeManagerHooks#removeOptionalRecipes(Map)} method.
     *
     * @param pObject          The {@link Map} of {@link ResourceLocation}s and {@link JsonElement} to edit.
     * @param pResourceManager The {@link ResourceManager} that handles the recipes. <strong>Unused in this
     *                         mixin.</strong>
     * @param pProfiler        The {@link ProfilerFiller} that is used during the method. <strong>Unused in this
     *                         mixin.</strong>
     * @param callback         Mixin's way of returning a method. <strong>Unused in this mixin.</strong>
     * @see RecipeManagerHooks#removeOptionalRecipes(Map)
     * @see RecipeManager#apply(Map, ResourceManager, ProfilerFiller)
     */
    @Inject(
            method = "apply(Ljava/lang/Object;Lnet/minecraft/server/packs/resources/ResourceManager;Lnet/minecraft/util/profiling/ProfilerFiller;)V",
            at = @At("HEAD")
    )
    @SuppressWarnings("unchecked")
    private void apply(Object pObject, ResourceManager pResourceManager, ProfilerFiller pProfiler, CallbackInfo callback)
    {
        RecipeManagerHooks.removeOptionalRecipes(((Map<ResourceLocation, JsonElement>) pObject));
    }
}
