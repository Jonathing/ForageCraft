package me.jonathing.minecraft.foragecraft.coremod.mixin;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * An alternative way of manually loading mixins without needing multiple mixin JSONs or the use of a
 * {@link org.spongepowered.asm.mixin.connect.IMixinConnector MixinConnector}.
 *
 * @author Jonathing
 * @since 2.2.2
 */
public class ForageMixinPlugin implements IMixinConfigPlugin
{
    private static final String[] MAIN_MIXINS = new String[]
            {
                    "EntityMixin",
                    "ItemMixin",
                    "RecipeManagerMixin"
            };

    @Override
    public void onLoad(String mixinPackage)
    {
    }

    @Override
    public String getRefMapperConfig()
    {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName)
    {
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets)
    {
    }

    @Override
    public List<String> getMixins()
    {
        return new ArrayList<>(Arrays.asList(MAIN_MIXINS));
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
    {
    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo)
    {
    }
}
