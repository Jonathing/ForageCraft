package me.jonathing.minecraft.foragecraft.coremod.mixin;

import me.jonathing.minecraft.foragecraft.coremod.hooks.ItemHooks;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.context.UseOnContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * This mixin class is used to make certain modifications to the {@link Item} class to ForageCraft's needs.
 *
 * @author Jonathing
 * @see Item
 * @since 2.1.0
 */
@Mixin(Item.class)
public class ItemMixin
{
    /**
     * This mixin method hooks into the {@link org.spongepowered.asm.mixin.injection.points.MethodHead MethodHead} of
     * the {@link Item#useOn(UseOnContext)} method to give the {@link UseOnContext} to the
     * {@link ItemHooks#useOn(UseOnContext)} method. If the result is not {@code null}, the mixin method sets the return
     * value to that method's result and cancels the method.
     *
     * @param pContext The item context in which the player right clicks any block with an item in hand.
     * @param callback Mixin's way of returning the resulting {@link InteractionResult} of the action.
     * @see ItemHooks#useOn(UseOnContext)
     * @see Item#useOn(UseOnContext)
     */
    @Inject(
            method = "useOn(Lnet/minecraft/world/item/context/UseOnContext;)Lnet/minecraft/world/InteractionResult;",
            at = @At("HEAD"),
            cancellable = true
    )
    private void useOn(UseOnContext pContext, CallbackInfoReturnable<InteractionResult> callback)
    {
        if (pContext != null && pContext.getItemInHand().getItem().equals(Items.STICK))
            callback.setReturnValue(ItemHooks.useOn(pContext));
    }
}
