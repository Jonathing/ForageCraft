package me.jonathing.minecraft.foragecraft.coremod.hooks;

import me.jonathing.minecraft.foragecraft.common.block.StickBlock;
import me.jonathing.minecraft.foragecraft.common.registry.ForageBlocks;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.AirBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraft.world.level.material.Fluids;

import javax.annotation.Nonnull;

/**
 * This class contains methods that are used by the {@code ItemCoreMod} coremod.
 *
 * @author Jonathing
 * @since 2.2.2
 */
public final class ItemHooks
{
    private ItemHooks()
    {
    }

    /**
     * The ForageCraft {@link ForageBlocks#stick} is a very special case. It does not have its own {@link BlockItem}
     * since we use the Minecraft {@link net.minecraft.world.item.Items#STICK Items.STICK} to place it down in the
     * world. This has led to issues in ForageCraft prior to version {@code 2.1.0} where right-clicking with the
     * Minecraft stick in hand could lead to unwanted behavior.
     * <p>
     * The solution? Hacking into the fucking game!
     * <p>
     * What I have done here is essentially copy-pasted a bunch of vanilla code that gives block items their
     * functionality, and I have tweaked it slightly to fit the ForageCraft's stick's needs. To summarize, the
     * {@code ItemCoreMod} coremod hooks into the
     * {@link net.minecraft.world.item.Item#useOn(UseOnContext) Item.useOn(ItemUseContext)} method to fool Minecraft
     * into believing that {@link net.minecraft.world.item.Items#STICK Items.STICK} has a {@link BlockItem}, which in
     * this case is the {@link ForageBlocks#stick}.
     *
     * @param itemUseContext The item context in which the player right clicks any block with an item in hand.
     * @see net.minecraft.world.item.Item#useOn(UseOnContext) Item.useOn(ItemUseContext)
     */
    @Nonnull
    public static InteractionResult useOn(UseOnContext itemUseContext)
    {
        BlockPlaceContext useContext = new BlockPlaceContext(itemUseContext);

        if (!canPlace(useContext))
            return InteractionResult.FAIL;
        else
        {
            BlockState blockState = ForageBlocks.stick.getStateForPlacement(useContext);
            blockState = blockState == null ? ((StickBlock) ForageBlocks.stick).getStateWithRandomDirection(itemUseContext.getLevel()) : blockState;
            if (!useContext.getLevel().setBlock(useContext.getClickedPos(), blockState, 11))
                return InteractionResult.FAIL;
            else
            {
                BlockPos pos = useContext.getClickedPos();
                Level world = useContext.getLevel();
                Player player = useContext.getPlayer();
                ItemStack itemStack = useContext.getItemInHand();
                BlockState blockStateInWorld = world.getBlockState(pos);
                Block block = blockStateInWorld.getBlock();
                if (block == blockState.getBlock())
                {
                    blockStateInWorld = updateBlockStateFromTag(pos, world, itemStack, blockStateInWorld);
                    BlockItem.updateCustomBlockEntityTag(world, player, pos, itemStack);
                    block.setPlacedBy(world, pos, blockStateInWorld, player, itemStack);
                    if (player instanceof ServerPlayer)
                        CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayer) player, pos, itemStack);
                }

                SoundType soundtype = blockStateInWorld.getSoundType(world, pos, useContext.getPlayer());
                world.playSound(player, pos, SoundEvents.WOOD_PLACE, SoundSource.BLOCKS, (soundtype.getVolume() + 1.0F) / 2.0F, soundtype.getPitch() * 0.8F);
                if (player == null || !player.getAbilities().instabuild)
                    itemStack.shrink(1);

                return InteractionResult.sidedSuccess(world.isClientSide);
            }
        }
    }

    /**
     * Checks with the {@link BlockPlaceContext#canPlace()} method along with some extra conditions for the
     * {@link ForageBlocks#stick} in particular.
     *
     * @return The result of the check.
     */
    private static boolean canPlace(BlockPlaceContext useContext)
    {
        Level world = useContext.getLevel();
        BlockPos pos = useContext.getClickedPos();
        return world.getBlockState(pos.below()).canOcclude()
                && (world.getBlockState(pos).getBlock() instanceof AirBlock
                || world.getBlockState(pos).getFluidState().equals(Fluids.WATER.getSource(false)))
                && useContext.canPlace();
    }

    /**
     * Copy-pasted vanilla code from the {@link BlockItem} class. For some reason this method isn't static, so I can't
     * access-transform it.
     *
     * @see BlockItem#updateBlockStateFromTag(BlockPos, Level, ItemStack, BlockState)
     */
    private static BlockState updateBlockStateFromTag(BlockPos pPos, Level pLevel, ItemStack pStack, BlockState pState)
    {
        BlockState blockstate = pState;
        CompoundTag compoundtag = pStack.getTag();
        if (compoundtag != null)
        {
            CompoundTag compoundtag1 = compoundtag.getCompound("BlockStateTag");
            StateDefinition<Block, BlockState> statedefinition = pState.getBlock().getStateDefinition();

            for (String s : compoundtag1.getAllKeys())
            {
                Property<?> property = statedefinition.getProperty(s);
                if (property != null)
                {
                    String s1 = compoundtag1.get(s).getAsString();
                    blockstate = BlockItem.updateState(blockstate, property, s1);
                }
            }
        }

        if (blockstate != pState)
            pLevel.setBlock(pPos, blockstate, 2);

        return blockstate;
    }
}
