package me.jonathing.minecraft.foragecraft.coremod.mixin;

import me.jonathing.minecraft.foragecraft.common.block.template.DecorativeBlock;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.ItemLike;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

/**
 * This mixin class is used to make certain modifications to the {@link Entity} class to ForageCraft's needs.
 *
 * @author Jonathing
 * @see Entity
 * @since 2.0.0
 */
@Mixin(Entity.class)
public abstract class EntityMixin
{
    /**
     * This method hooks into the {@link org.spongepowered.asm.mixin.injection.points.BeforeInvoke BeforeInvoke} call of
     * the {@link Entity#spawnAtLocation(ItemLike, int)} method in the
     * {@link Entity#spawnAtLocation(ItemLike)} to modify the {@link ItemLike} argument of the method. If the argument
     * is an instance of a {@link DecorativeBlock}, the argument to be passed will instead be the value returned by
     * {@link DecorativeBlock#getDecorativeItem()}.
     *
     * @see Entity#spawnAtLocation(ItemLike)
     * @see Entity#spawnAtLocation(ItemLike, int)
     */
    @ModifyArg(
            method = "spawnAtLocation(Lnet/minecraft/world/level/ItemLike;)Lnet/minecraft/world/entity/item/ItemEntity;",
            at = @At(
                    value = "INVOKE",
                    target = "net/minecraft/world/entity/Entity.spawnAtLocation(Lnet/minecraft/world/level/ItemLike;I)Lnet/minecraft/world/entity/item/ItemEntity;"
            )
    )
    private ItemLike modify$pItem(ItemLike pItem)
    {
        return pItem instanceof DecorativeBlock ? ((DecorativeBlock) pItem).getDecorativeItem() : pItem;
    }
}
