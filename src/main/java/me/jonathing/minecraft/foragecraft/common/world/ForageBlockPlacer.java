package me.jonathing.minecraft.foragecraft.common.world;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.Tag;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.blockplacers.SimpleBlockPlacer;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Random;

/**
 * The forage block placer is essentially a {@link SimpleBlockPlacer} but uses a block {@link Tag} as a special
 * whitelist for placing blocks.
 *
 * @author Jonathing
 * @see SimpleBlockPlacer
 * @since 2.2.0
 */
@ParametersAreNonnullByDefault
public class ForageBlockPlacer extends SimpleBlockPlacer
{
    private final Tag<Block> blockTag;

    public ForageBlockPlacer(Tag<Block> blockTag)
    {
        this.blockTag = blockTag;
    }

    @Override
    public void place(LevelAccessor level, BlockPos blockPos, BlockState blockState, Random random)
    {
        if (blockState.hasProperty(HorizontalDirectionalBlock.FACING))
            blockState = blockState.setValue(HorizontalDirectionalBlock.FACING, Direction.Plane.HORIZONTAL.getRandomDirection(level.getRandom()));

        if (level.getBlockState(blockPos.below()).is(this.blockTag))
            super.place(level, blockPos, blockState, random);
    }
}
