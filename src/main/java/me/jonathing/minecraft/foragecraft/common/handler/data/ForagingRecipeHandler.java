package me.jonathing.minecraft.foragecraft.common.handler.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;
import me.jonathing.minecraft.foragecraft.common.handler.ForagingEventHandler;
import me.jonathing.minecraft.foragecraft.data.objects.ForagingRecipe;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.profiling.ProfilerFiller;

import javax.annotation.Nonnull;
import java.util.Map;

public class ForagingRecipeHandler extends ForageDataHandler<ResourceLocation, ForagingRecipe>
{
    public ForagingRecipeHandler()
    {
        super(ForagingRecipe.DIRECTORY);
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> jsonMap, @Nonnull ResourceManager resourceManager, @Nonnull ProfilerFiller profiler)
    {
        super.apply(jsonMap, resourceManager, profiler);
        ForagingEventHandler.reloadDrops(this.getData());
    }

    public Pair<CompoundTag, CompoundTag> entryToNBT(ResourceLocation key, ForagingRecipe value)
    {
        CompoundTag keyNbt = new CompoundTag();
        keyNbt.putString("name", key.toString());
        return Pair.of(keyNbt, value.toNBT());
    }

    public Pair<ResourceLocation, ForagingRecipe> entryFromNbt(CompoundTag key, CompoundTag value)
    {
        return Pair.of(new ResourceLocation(key.getString("name")), ForagingRecipe.fromNBT(value));
    }

    @Override
    protected Pair<ResourceLocation, ForagingRecipe> parseJson(JsonObject json, ResourceLocation name) throws MissingRegistryObjectException
    {
        return Pair.of(name, ForagingRecipe.fromJson(json));
    }
}
