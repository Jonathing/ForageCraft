@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
package me.jonathing.minecraft.foragecraft.common.block;

import net.minecraft.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
