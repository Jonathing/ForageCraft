@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
package me.jonathing.minecraft.foragecraft.common.block.template;

import net.minecraft.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
