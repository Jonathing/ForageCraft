package me.jonathing.minecraft.foragecraft.common.capability.base;

import me.jonathing.minecraft.foragecraft.common.capability.util.IPersistentCapability;

public interface IForageChunk extends IPersistentCapability<IForageChunk>
{
    void forage();

    int getTimesForaged();

    void setTimesForaged(int timesForaged);
}
