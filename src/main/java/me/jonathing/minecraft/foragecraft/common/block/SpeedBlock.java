package me.jonathing.minecraft.foragecraft.common.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;

/**
 * A speed block is just like a normal {@link Block} but it speeds up any entities that step on it by a certain
 * multiplier {@link #speedMultiplier} given in the {@link #SpeedBlock(float, Properties)} constructor.
 *
 * @author Jonathing
 * @see #stepOn(Level, BlockPos, BlockState, Entity)
 * @see Block
 * @since 2.0.0
 */
public class SpeedBlock extends Block
{
    private final float speedMultiplier;

    public SpeedBlock(float speedMultiplier, BlockBehaviour.Properties properties)
    {
        super(properties);
        this.speedMultiplier = speedMultiplier;
    }

    /**
     * This method ensures that whenever an entity is walking on a speed block, they move at {@link #speedMultiplier}
     * times their normal speed.
     *
     * @param level    The level in which the speed block exists.
     * @param blockPos The position in the level where the speed block exists.
     * @param entity   The entity that is stepping on the block.
     * @see Block#stepOn(Level, BlockPos, BlockState, Entity)
     */
    @Override
    public void stepOn(Level level, BlockPos blockPos, BlockState state, Entity entity)
    {
        super.stepOn(level, blockPos, state, entity);
        entity.setDeltaMovement(entity.getDeltaMovement().x * this.speedMultiplier, 0, entity.getDeltaMovement().z * this.speedMultiplier);
    }
}
