package me.jonathing.minecraft.foragecraft.common.capability.util;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;

public interface IPersistentCapability<C>
{
    Capability<C> getDefaultInstance();

    CompoundTag writeAdditional(CompoundTag nbt);

    default CompoundTag writeAdditional()
    {
        CompoundTag nbt = new CompoundTag();
        return this.writeAdditional(nbt);
    }

    void read(CompoundTag nbt);
}
