package me.jonathing.minecraft.foragecraft.common.block;

import me.jonathing.minecraft.foragecraft.common.block.template.DecorativeBlock;
import me.jonathing.minecraft.foragecraft.common.registry.ForageBlocks;
import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.util.Lazy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Random;
import java.util.function.Supplier;

/**
 * This class holds the {@link ForageBlocks#stick} block. It is required so that it is able to have its own hitbox,
 * along with several other features that are exclusive to the stick block.
 *
 * @author Jonathing
 * @see ForageBlocks#stick
 * @see RockBlock
 * @since 2.0.0
 */
public class StickBlock extends DecorativeBlock
{
    public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;

    private static final Supplier<VoxelShape> SHAPE = () ->
            Block.box(1.0D, 0.0D, 1.0D, 15.0D, 1.0D, 15.0D);
    private static final Supplier<Properties> PROPERTIES =
            () -> BlockBehaviour.Properties
                    .copy(Blocks.OAK_PLANKS)
                    .noCollission()
                    .noOcclusion()
                    .instabreak();

    /**
     * Makes a new {@link DecorativeBlock} with features exclusive to the {@link ForageBlocks#stick}.
     *
     * @see DecorativeBlock#DecorativeBlock(Properties, VoxelShape, Lazy)
     */
    public StickBlock()
    {
        super(PROPERTIES.get(), SHAPE.get(), () -> Items.STICK);
    }

    @Override
    @SuppressWarnings("deprecation")
    public BlockState rotate(BlockState state, Rotation rot)
    {
        return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    @SuppressWarnings("deprecation")
    public BlockState mirror(BlockState state, Mirror mirrorIn)
    {
        return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        builder.add(RockBlock.WATERLOGGED, FACING);
    }

    /**
     * Contains the same logic from {@link RockBlock#getStateForPlacement(BlockPlaceContext)} but also accounts for the
     * {@link StickBlock#FACING} property. Can be {@code null} if a {@link BlockState} for placement cannot be found.
     *
     * @param context The item use context given to the method.
     * @return Either a waterlogged or non-waterlogged stick based on the result of this method.
     */
    @Override
    @Nullable
    public BlockState getStateForPlacement(BlockPlaceContext context)
    {
        FluidState fluidstate = context.getLevel().getFluidState(context.getClickedPos());

        for (Direction direction : context.getNearestLookingDirections())
        {
            if (direction.getAxis() == Direction.Axis.Y)
            {
                BlockState blockstate = this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
                if (blockstate.canSurvive(context.getLevel(), context.getClickedPos()))
                    return blockstate.setValue(WATERLOGGED, fluidstate.getType() == Fluids.WATER);
            }
        }

        return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    /**
     * Calls {@link #getStateWithRandomDirection(Random)} using a given {@link LevelAccessor}'s
     * {@link LevelAccessor#getRandom()}.
     *
     * @param level The level to use its {@link LevelAccessor#getRandom()} for.
     * @return The blockstate given by {@link #getStateWithRandomDirection(Random)} using the level's random.
     * @see LevelAccessor#getRandom()
     */
    public BlockState getStateWithRandomDirection(LevelAccessor level)
    {
        return this.getStateWithRandomDirection(level.getRandom());
    }

    /**
     * Gets the stick block's default {@link BlockState} along with a random {@link Direction}. It is preferable to use
     * this rather than {@link Block#defaultBlockState()}.
     *
     * @param random The {@link Random} for determining which {@link Direction} to use. It is recommended to use a
     *               {@link net.minecraft.world.level.Level#random Level.random}.
     * @return {@link Block#defaultBlockState()} with a random {@link Direction}.
     * @see Block#defaultBlockState()
     * @see #getStateWithRandomDirection(LevelAccessor)
     */
    public BlockState getStateWithRandomDirection(@Nonnull Random random)
    {
        return this.defaultBlockState().setValue(FACING, Direction.Plane.HORIZONTAL.getRandomDirection(random));
    }

    @Override
    public Item asItem()
    {
        return this.getDecorativeItem();
    }
}
