package me.jonathing.minecraft.foragecraft.common.capability.util;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;

public class CapabilityProvider<C extends IPersistentCapability<C>> implements ICapabilitySerializable<CompoundTag>
{
    private final LazyOptional<C> capabilityHandler;
    private final Lazy<Capability<C>> instance;

    public CapabilityProvider(C capability)
    {
        this.capabilityHandler = LazyOptional.of(() -> capability);
        this.instance = capability::getDefaultInstance;
    }

    @Override
    @Nonnull
    public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
    {
        return cap == this.instance.get() ? this.capabilityHandler.cast() : LazyOptional.empty();
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        this.capabilityHandler.orElse(null).read(nbt);
    }

    @Override
    public CompoundTag serializeNBT()
    {
        return this.capabilityHandler.orElse(null).writeAdditional();
    }
}
