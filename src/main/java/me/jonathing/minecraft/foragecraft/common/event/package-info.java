@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
package me.jonathing.minecraft.foragecraft.common.event;

import net.minecraft.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
