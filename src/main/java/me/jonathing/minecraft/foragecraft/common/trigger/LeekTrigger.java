package me.jonathing.minecraft.foragecraft.common.trigger;

import com.google.gson.JsonObject;
import me.jonathing.minecraft.foragecraft.ForageCraft;
import net.minecraft.advancements.critereon.AbstractCriterionTriggerInstance;
import net.minecraft.advancements.critereon.DeserializationContext;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

/**
 * This is the custom trigger that is exclusive to the triple baka goal advancement.
 *
 * @author Jonathing
 * @see me.jonathing.minecraft.foragecraft.common.item.LeekItem#hurtEnemy(ItemStack, LivingEntity, LivingEntity)
 * @since 2.1.2
 */
public class LeekTrigger extends SimpleCriterionTrigger<LeekTrigger.Instance>
{
    private static final ResourceLocation ID = ForageCraft.locate("leek_trigger");

    @Override
    public ResourceLocation getId()
    {
        return ID;
    }

    @Override
    protected LeekTrigger.Instance createInstance(JsonObject json, EntityPredicate.Composite entityPredicate, DeserializationContext conditionsParser)
    {
        return new Instance(entityPredicate);
    }

    /**
     * Triggers the {@link LeekTrigger} for a specific {@link ServerPlayer} when the
     * {@link me.jonathing.minecraft.foragecraft.common.item.LeekItem#hurtEnemy(ItemStack, LivingEntity, LivingEntity) LeekItem.hurtEnemy(ItemStack, LivingEntity, LivingEntity)}
     * method is called.
     *
     * @param player The player that caused the trigger by hitting an entity with a leek.
     * @see me.jonathing.minecraft.foragecraft.common.item.LeekItem#hurtEnemy(ItemStack, LivingEntity, LivingEntity) LeekItem.hurtEnemy(ItemStack, LivingEntity, LivingEntity)
     */
    public void trigger(ServerPlayer player)
    {
        this.trigger(player, (instance) -> true);
    }

    public static class Instance extends AbstractCriterionTriggerInstance
    {
        public Instance(EntityPredicate.Composite playerCondition)
        {
            super(ID, playerCondition);
        }

        /**
         * Creates a raw {@link LeekTrigger.Instance} for use in data generation.
         *
         * @return The created {@link LeekTrigger.Instance} with the given parameters.
         * @see LeekTrigger.Instance
         */
        public static Instance create()
        {
            return new Instance(EntityPredicate.Composite.ANY);
        }
    }
}
