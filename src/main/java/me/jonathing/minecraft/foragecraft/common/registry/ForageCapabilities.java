package me.jonathing.minecraft.foragecraft.common.registry;

import me.jonathing.minecraft.foragecraft.ForageCraft;
import me.jonathing.minecraft.foragecraft.common.capability.ForageChunk;
import me.jonathing.minecraft.foragecraft.common.capability.base.IForageChunk;
import me.jonathing.minecraft.foragecraft.common.capability.util.CapabilityProvider;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;

/**
 * This class holds all of the capabilities in ForageCraft.
 *
 * @author Jonathing
 * @see #registerCapabilities(RegisterCapabilitiesEvent)
 * @since 2.2.2
 */
public class ForageCapabilities
{
    @CapabilityInject(IForageChunk.class)
    public static Capability<IForageChunk> CHUNK = null;

    static void registerCapabilities(RegisterCapabilitiesEvent event)
    {
        event.register(IForageChunk.class);
    }

    static void onAttachChunkCapability(AttachCapabilitiesEvent<LevelChunk> event)
    {
        event.addCapability(ForageCraft.locate("foraged_chunk"), new CapabilityProvider<>(new ForageChunk()));
    }
}
