package me.jonathing.minecraft.foragecraft.common.trigger;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import me.jonathing.minecraft.foragecraft.ForageCraft;
import me.jonathing.minecraft.foragecraft.common.util.JsonUtil;
import net.minecraft.advancements.critereon.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.event.world.BlockEvent;

import javax.annotation.ParametersAreNullableByDefault;
import java.util.List;
import java.util.function.Consumer;

/**
 * This is the custom trigger that is exclusive to forage drops.
 *
 * @author Jonathing
 * @see me.jonathing.minecraft.foragecraft.common.handler.ForagingEventHandler#forageDrop(List, BlockEvent.BreakEvent)
 * @since 2.1.0
 */
public class ForagingTrigger extends SimpleCriterionTrigger<ForagingTrigger.Instance>
{
    private static final ResourceLocation ID = ForageCraft.locate("foraging_trigger");
    private static final String BLOCK_KEY = "block";
    private static final String ITEM_KEY = "item";

    @Override
    public ResourceLocation getId()
    {
        return ID;
    }

    @Override
    protected Instance createInstance(JsonObject json, EntityPredicate.Composite entityPredicate, DeserializationContext conditionsParser)
    {
        Block block = JsonUtil.Reader.fromRegistry(Block.class, json, BLOCK_KEY).orElseThrow(() -> new JsonSyntaxException("Unknown block type!"));
        ItemLike item = JsonUtil.Reader.fromRegistry(Item.class, json, ITEM_KEY).orElseThrow(() -> new JsonSyntaxException("Unknown item type!"));
        return new Instance(entityPredicate, block, item);
    }

    /**
     * Triggers the {@link ForagingTrigger} with a specific {@link Block} and {@link ItemLike} along with the
     * responsible {@link ServerPlayer}.
     *
     * @param playerEntity The {@link ServerPlayer} that caused the trigger via a forage drop.
     * @param block        The {@link Block} that the player broke.
     * @param item         The {@link Item} that was foraged from the broken block.
     * @see me.jonathing.minecraft.foragecraft.common.handler.ForagingEventHandler#forageDrop(List, BlockEvent.BreakEvent)
     * ForagingEventHandler.forageDrop(List, BlockEvent.BreakEvent)
     */
    public void trigger(ServerPlayer playerEntity, Block block, ItemLike item)
    {
        this.trigger(playerEntity, (instance) -> instance.test(block, item));
    }

    /**
     * This static inner class contains information about an instance of a {@link ForagingTrigger}. This is mainly used
     * for advancements.
     *
     * @see me.jonathing.minecraft.foragecraft.data.provider.ForageAdvancementProvider.ForageAdvancements#accept(Consumer)
     * ForageAdvancementProvider.ForageAdvancements.accept(Consumer)
     */
    public static class Instance extends AbstractCriterionTriggerInstance
    {
        private final Block block;
        private final ItemLike item;

        public Instance(EntityPredicate.Composite entityPredicate, Block block, ItemLike item)
        {
            super(ID, entityPredicate);
            this.block = block;
            this.item = item;
        }

        /**
         * Is used by {@link #trigger(ServerPlayer, Block, ItemLike)} to test if the trigger's {@link Block} and
         * {@link Item} match that of this {@link Instance}'s.
         *
         * @param block The trigger's {@link Block}.
         * @param item  The trigger's {@link Item}.
         * @return The result of the test.
         * @see #trigger(ServerPlayer, Block, ItemLike)
         */
        @ParametersAreNullableByDefault
        protected boolean test(Block block, ItemLike item)
        {
            if (block == null || item == null) return false;

            return this.block.equals(block) && this.item.asItem().equals(item.asItem());
        }

        /**
         * Creates a raw {@link Instance} for use in data generation.
         *
         * @param block The {@link Block} to be used for the instance.
         * @param item  The {@link Item} to be used for the instance.
         * @return The created {@link Instance} with the given parameters.
         * @see Instance
         */
        public static Instance create(Block block, ItemLike item)
        {
            return new Instance(EntityPredicate.Composite.ANY, block, item);
        }

        @Override
        public JsonObject serializeToJson(SerializationContext conditionSerializer)
        {
            JsonObject jsonObject = super.serializeToJson(conditionSerializer);
            JsonUtil.Writer.fromRegistry(Block.class, this.block, jsonObject, BLOCK_KEY);
            JsonUtil.Writer.fromRegistry(Item.class, this.item.asItem(), jsonObject, ITEM_KEY);
            return jsonObject;
        }
    }
}
