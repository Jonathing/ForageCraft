package me.jonathing.minecraft.foragecraft.common.registry;

import me.jonathing.minecraft.foragecraft.ForageCraft;
import me.jonathing.minecraft.foragecraft.ForageInfo;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.fmllegacy.DatagenModLoader;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * This class holds all of the item groups in ForageCraft.
 * <p>
 * Currently, there is only <strong>one</strong> item group in ForageCraft, and it is <em>only</em> used in a
 * development environment.
 *
 * @author Jonathing
 * @see CreativeModeTab
 * @since 2.0.0
 */
public class ForageItemGroups extends CreativeModeTab
{
    /**
     * This is the development environment item group for ForageCraft. This will <em>never</em> be seen in game unless
     * the specific system property has been defined.
     */
    @Nullable
    public static final ForageItemGroups FORAGECRAFT = ForageInfo.IDE && !DatagenModLoader.isRunningDataGen() ? new ForageItemGroups(ForageInfo.MOD_ID, "stick_bundle") : null;
    private final String iconName;

    public ForageItemGroups(String label, String iconName)
    {
        super(label);
        this.iconName = iconName;
    }

    /**
     * This method creates an icon for a specific item group. In this case,  only the dev-environment group is needed
     * here.
     *
     * @see CreativeModeTab#makeIcon()
     * @see #FORAGECRAFT
     */
    @Override
    @Nonnull
    public ItemStack makeIcon()
    {
        return new ItemStack(ForgeRegistries.ITEMS.getValue(ForageCraft.locate(this.iconName)));
    }

    @Nonnull
    static CreativeModeTab getItemGroup(CreativeModeTab itemGroup)
    {
        return FORAGECRAFT == null ? itemGroup : FORAGECRAFT;
    }
}
