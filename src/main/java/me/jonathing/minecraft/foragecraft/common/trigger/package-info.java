@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
package me.jonathing.minecraft.foragecraft.common.trigger;

import net.minecraft.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
