package me.jonathing.minecraft.foragecraft.data.provider;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.jonathing.minecraft.foragecraft.ForageCraft;
import me.jonathing.minecraft.foragecraft.ForageInfo;
import me.jonathing.minecraft.foragecraft.common.registry.ForageBlocks;
import me.jonathing.minecraft.foragecraft.common.registry.ForageItems;
import me.jonathing.minecraft.foragecraft.common.trigger.ForagingTrigger;
import me.jonathing.minecraft.foragecraft.common.trigger.LeekTrigger;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.HashCache;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import static me.jonathing.minecraft.foragecraft.data.ForageCraftData.LOGGER;

/**
 * The provider for all of the advancements in ForageCraft.
 *
 * @author Jonathing
 * @author Silver_David
 * @see DataProvider
 * @since 2.1.0
 */
public class ForageAdvancementProvider implements DataProvider
{
    private static final Marker MARKER = MarkerManager.getMarker("Advancements");
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();
    private final DataGenerator generator;
    private final List<Consumer<Consumer<Advancement>>> advancements = ImmutableList.of(new ForageAdvancements());

    public ForageAdvancementProvider(DataGenerator generator)
    {
        this.generator = generator;
    }

    @Override
    public void run(@Nonnull HashCache cache) throws IOException
    {
        Path path = this.generator.getOutputFolder();
        Set<ResourceLocation> set = Sets.newHashSet();
        Consumer<Advancement> advancement = (a) ->
        {
            if (!set.add(a.getId()))
                throw new IllegalStateException("Duplicate advancement " + a.getId());
            else
            {
                Path path1 = getPath(path, a);

                try
                {
                    DataProvider.save(GSON, cache, a.deconstruct().serializeToJson(), path1);
                }
                catch (IOException ioexception)
                {
                    LOGGER.error(MARKER, String.format("Couldn't save advancement %s", path1), ioexception);
                }

            }
        };

        this.advancements.forEach(adv -> adv.accept(advancement));
    }

    private static Path getPath(Path path, Advancement advancement)
    {
        return path.resolve("data/" + advancement.getId().getNamespace() + "/advancements/" + advancement.getId().getPath() + ".json");
    }

    @Override
    @Nonnull
    public String getName()
    {
        return "ForageCraft Advancements";
    }

    private class ForageAdvancements implements Consumer<Consumer<Advancement>>
    {
        private final String section = "";

        @Override
        @SuppressWarnings("unused")
        public void accept(Consumer<Advancement> consumer)
        {
            // ForageCraft advancements
            Advancement root = this.builder(ForageItems.stick_bundle, "root", ForageCraft.locate("textures/block/fascine_side.png"), FrameType.TASK, false, false, false)
                    .addCriterion("has_" + Blocks.DIRT.asItem().getRegistryName().getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(Blocks.DIRT.asItem()))
                    .save(consumer, ForageCraft.find("main/root"));

            Advancement root_vegetable = this.builder(Items.POTATO, "foraged_root_vegetable", FrameType.TASK, true, false, false)
                    .parent(root)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_potato_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.POTATO))
                    .addCriterion("foraged_potato_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.POTATO))
                    .addCriterion("foraged_carrot_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.CARROT))
                    .addCriterion("foraged_beetroot_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.BEETROOT))
                    .save(consumer, ForageCraft.find("main/foraged_root_vegetable"));

            Advancement poisonous_potato = this.builder(Items.POISONOUS_POTATO, "foraged_poisonous_potato", FrameType.TASK, true, false, false)
                    .parent(root_vegetable)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_poisonous_potato_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.POISONOUS_POTATO))
                    .addCriterion("foraged_poisonous_potato_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.POISONOUS_POTATO))
                    .save(consumer, ForageCraft.find("main/foraged_poisonous_potato"));

            Advancement flint = this.builder(Items.FLINT, "foraged_flint", FrameType.TASK, true, false, false)
                    .parent(root)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_flint_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.FLINT))
                    .addCriterion("foraged_flint_from_stone", ForagingTrigger.Instance.create(Blocks.STONE, Items.FLINT))
                    .save(consumer, ForageCraft.find("main/foraged_flint"));

            Advancement bone = this.builder(Items.BONE, "foraged_bone", FrameType.TASK, true, true, false)
                    .parent(flint)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_bone_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.BONE))
                    .addCriterion("foraged_bone_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.BONE))
                    .save(consumer, ForageCraft.find("main/foraged_bone"));

            Advancement skeleton_skull = this.builder(Items.SKELETON_SKULL, "foraged_skeleton_skull", FrameType.TASK, true, true, false)
                    .parent(bone)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_skeleton_skull_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.SKELETON_SKULL))
                    .addCriterion("foraged_skeleton_skull_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.SKELETON_SKULL))
                    .save(consumer, ForageCraft.find("main/foraged_skeleton_skull"));

            Advancement gold_nugget = this.builder(Items.GOLD_NUGGET, "foraged_gold_nugget", FrameType.TASK, true, true, false)
                    .parent(bone)
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_gold_nugget_from_stone", ForagingTrigger.Instance.create(Blocks.STONE, Items.GOLD_NUGGET))
                    .addCriterion("foraged_gold_nugget_from_nether_quartz_ore", ForagingTrigger.Instance.create(Blocks.NETHER_QUARTZ_ORE, Items.GOLD_NUGGET))
                    .save(consumer, ForageCraft.find("main/foraged_gold_nugget"));

            Advancement diamond = this.builder(Items.DIAMOND, "foraged_diamond", FrameType.CHALLENGE, true, true, false)
                    .parent(gold_nugget)
                    .addCriterion("foraged_diamond_from_coal_ore", ForagingTrigger.Instance.create(Blocks.COAL_ORE, Items.DIAMOND))
                    .save(consumer, ForageCraft.find("main/foraged_diamond"));

            Advancement emerald = this.builder(Items.EMERALD, "foraged_emerald", FrameType.CHALLENGE, true, true, false)
                    .parent(gold_nugget)
                    .addCriterion("foraged_emerald_from_coal_ore", ForagingTrigger.Instance.create(Blocks.COAL_ORE, Items.EMERALD))
                    .save(consumer, ForageCraft.find("main/foraged_emerald"));

            Advancement gathering_knife = this.haveAnyItem(this.builder(ForageItems.gathering_knife, "has_gathering_knife", FrameType.TASK, true, false, false)
                            .parent(root),
                    ImmutableList.of(ForageItems.gathering_knife))
                    .save(consumer, ForageCraft.find("main/has_gathering_knife"));

            Advancement straw = this.haveAnyItem(this.builder(ForageItems.straw, "has_straw", FrameType.TASK, true, false, false)
                            .parent(gathering_knife),
                    ImmutableList.of(ForageItems.straw))
                    .save(consumer, ForageCraft.find("main/has_straw"));

            Advancement seeds_stolen = this.haveAnyItem(this.builder(ForageItems.leek_seeds, "has_leek_seeds", FrameType.TASK, true, true, false)
                            .parent(root),
                    ImmutableList.of(ForageItems.leek_seeds))
                    .save(consumer, ForageCraft.find("main/has_leek_seeds"));

            Advancement leek = this.haveAnyItem(this.builder(ForageItems.leek, "has_leek", FrameType.TASK, true, true, false)
                            .parent(seeds_stolen),
                    ImmutableList.of(ForageItems.leek))
                    .save(consumer, ForageCraft.find("main/has_leek"));

            Advancement baka_baka_baka = this.builder(ForageItems.leek, "triple_baka", FrameType.GOAL, true, true, true)
                    .parent(leek)
                    .addCriterion("hit_entity_with_leek", LeekTrigger.Instance.create())
                    .save(consumer, ForageCraft.find("main/triple_baka"));

            Advancement spaghetti = this.haveAnyItem(this.builder(ForageItems.spaghetti, "has_spaghetti", FrameType.GOAL, true, true, true)
                            .parent(root),
                    ImmutableList.of(ForageItems.spaghetti))
                    .save(consumer, ForageCraft.find("main/has_spaghetti"));

            // Patchouli book exclusives
            Advancement book_underground = Advancement.Builder.advancement()
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("forage_bone_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.BONE))
                    .addCriterion("forage_bone_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.BONE))
                    .addCriterion("forage_skeleton_skull_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Blocks.SKELETON_SKULL))
                    .addCriterion("forage_skeleton_skull_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Blocks.SKELETON_SKULL))
                    .addCriterion("forage_flint_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.FLINT))
                    .addCriterion("forage_flint_from_stone", ForagingTrigger.Instance.create(Blocks.STONE, Items.FLINT))
                    .addCriterion("forage_gold_nugget_from_stone", ForagingTrigger.Instance.create(Blocks.STONE, Items.GOLD_NUGGET))
                    .addCriterion("forage_diamond_from_coal_ore", ForagingTrigger.Instance.create(Blocks.COAL_ORE, Items.DIAMOND))
                    .addCriterion("forage_emerald_from_coal_ore", ForagingTrigger.Instance.create(Blocks.COAL_ORE, Items.EMERALD))
                    .save(consumer, ForageCraft.find("book_exclusive/underground"));

            Advancement book_root_vegetables = Advancement.Builder.advancement()
                    .requirements(RequirementsStrategy.OR)
                    .addCriterion("foraged_potato_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.POTATO))
                    .addCriterion("foraged_potato_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.POTATO))
                    .addCriterion("foraged_carrot_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.CARROT))
                    .addCriterion("foraged_beetroot_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.BEETROOT))
                    .addCriterion("foraged_poisonous_potato_from_grass_block", ForagingTrigger.Instance.create(Blocks.GRASS_BLOCK, Items.POISONOUS_POTATO))
                    .addCriterion("foraged_poisonous_potato_from_dirt", ForagingTrigger.Instance.create(Blocks.DIRT, Items.POISONOUS_POTATO))
                    .save(consumer, ForageCraft.find("book_exclusive/root_vegetables"));

            Advancement book_paving_stones = Advancement.Builder.advancement()
                    .addCriterion("has_paving_stones", InventoryChangeTrigger.TriggerInstance.hasItems(ForageBlocks.paving_stones))
                    .save(consumer, ForageCraft.find("book_exclusive/has_paving_stones"));
        }

        private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
        {
            return Advancement.Builder.advancement().display(displayItem, this.translate(name), this.translate(name + ".desc"), background, frameType, showToast, announceToChat, hidden);
        }

        private Advancement.Builder builder(ItemLike displayItem, String name, FrameType frameType, boolean showToast, boolean announceToChat, boolean hidden)
        {
            return this.builder(displayItem, name, null, frameType, showToast, announceToChat, hidden);
        }

        private Advancement.Builder haveAnyItem(Advancement.Builder builder, List<ItemLike> items)
        {
            items.forEach(item -> builder.addCriterion("has_" + item.asItem().getRegistryName().getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(item)));
            return builder;
        }

        private TranslatableComponent translate(String key)
        {
            return new TranslatableComponent("advancements." + ForageInfo.MOD_ID + (this.section.equals("") ? "" : "." + this.section) + "." + key);
        }
    }
}
