package me.jonathing.minecraft.foragecraft.data.provider;

import com.google.gson.JsonObject;
import me.jonathing.minecraft.foragecraft.ForageCraft;
import me.jonathing.minecraft.foragecraft.common.registry.ForageBlocks;
import me.jonathing.minecraft.foragecraft.common.registry.ForageItems;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.HashCache;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * The provider for all of the recipes in ForageCraft.
 *
 * @author Jonathing
 * @author Silver_David
 * @see RecipeProvider
 * @since 2.0.0
 */
public class ForageRecipeProvider extends RecipeProvider
{
    private final String hasItem = "has_item";
    private Consumer<FinishedRecipe> consumer;

    public ForageRecipeProvider(DataGenerator generatorIn)
    {
        super(generatorIn);
    }

    /**
     * This method runs through all of the recipes within it and generates data based off of them.
     *
     * @see RecipeProvider#buildCraftingRecipes(Consumer)
     */
    @Override
    @ParametersAreNonnullByDefault
    protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer)
    {
        this.consumer = consumer;

        this.simpleInOut(Items.SKELETON_SKULL, Items.BONE_MEAL, 9);

        this.simple3x3withResourceLoc(ForageBlocks.rock, Items.COBBLESTONE);
        this.simpleInOut(Items.COBBLESTONE, ForageBlocks.rock, 9);

        this.simple3x3(ForageBlocks.flat_rock, ForageBlocks.paving_stones);

        this.simpleInOut(ForageBlocks.fascine, ForageItems.stick_bundle, 9);
        this.simple3x3(ForageItems.stick_bundle, ForageBlocks.fascine);

        // TODO: Scarecrow. Kill me.
//        ShapedRecipeBuilder.shapedRecipe(ForageBlocks.scarecrow, 1)
//                .key('l', Items.LEATHER).key('p', Items.PUMPKIN).key('f', ForageItems.stick_bundle).key('b', ForageBlocks.straw_bale).key('s', Items.STICK)
//                .patternLine("lpl").patternLine("fbf").patternLine("lsl").addCriterion(hasItem, hasItem(Items.PUMPKIN)).build(consumer);

        ShapedRecipeBuilder.shaped(ForageItems.spaghetti, 1)
                .define('s', Items.COOKED_BEEF).define('w', Items.WHEAT).define('b', Items.BOWL)
                .pattern(" s ").pattern("www").pattern(" b ").unlockedBy(this.hasItem, has(Items.WHEAT)).save(consumer);

        this.simple3x3(Items.STICK, ForageItems.stick_bundle);
        this.simpleInOut(ForageItems.stick_bundle, Items.STICK, 9);

        this.simple3x3(ForageItems.straw, ForageBlocks.straw_bale);
        this.simpleInOut(ForageBlocks.straw_bale, ForageItems.straw, 9);

        this.simpleInOut(ForageItems.leek, ForageItems.leek_seeds, 1);

        ShapedRecipeBuilder.shaped(ForageItems.gathering_knife, 1)
                .define('s', Items.STICK).define('r', ForageBlocks.flat_rock)
                .pattern(" r").pattern("s ").unlockedBy(this.hasItem, has(ForageBlocks.flat_rock.asItem())).save(consumer);

        ShapelessRecipeBuilder.shapeless(Items.STICK, 1)
                .requires(ItemTags.SAPLINGS)
                .requires(ForageItems.gathering_knife)
                .unlockedBy(this.hasItem, has(ForageItems.gathering_knife))
                .save(consumer, ForageCraft.find("stick_from_gathering_knife_and_sapling"));
    }

    private void simpleInOut(ItemLike in, ItemLike out, int amount)
    {
        this.simpleInOut(in, out, amount, in);
    }

    private void simpleInOut(ItemLike in, ItemLike out, int amount, ItemLike criterion)
    {
        ShapelessRecipeBuilder.shapeless(out, amount)
                .requires(in)
                .unlockedBy(this.hasItem, has(criterion))
                .save(this.consumer, ForageCraft.find(out.asItem().getRegistryName().getPath() + "_from_" + in.asItem().getRegistryName().getPath()));
    }

    private void simple3x3withResourceLoc(ItemLike in, ItemLike out, int amount)
    {
        ShapedRecipeBuilder.shaped(out, amount).define('#', in).pattern("###").pattern("###").pattern("###").unlockedBy(this.hasItem, has(in)).save(this.consumer, ForageCraft.find(out.asItem().getRegistryName().getPath() + "_from_" + in.asItem().getRegistryName().getPath()));
    }

    private void simple3x3withResourceLoc(ItemLike in, ItemLike out)
    {
        this.simple3x3withResourceLoc(in, out, 1);
    }

    private void simple3x3(ItemLike in, ItemLike out, int amount)
    {
        ShapedRecipeBuilder.shaped(out, amount).define('#', in).pattern("###").pattern("###").pattern("###").unlockedBy(this.hasItem, has(in)).save(this.consumer);
    }

    private void simple3x3(ItemLike in, ItemLike out)
    {
        this.simple3x3(in, out, 1);
    }

    @Override
    protected void saveAdvancement(HashCache p_208310_0_, JsonObject p_208310_1_, Path p_208310_2_)
    {
        super.saveAdvancement(p_208310_0_, p_208310_1_, p_208310_2_);
    }

    @Override
    @Nonnull
    public String getName()
    {
        return "ForageCraft Recipes";
    }
}
