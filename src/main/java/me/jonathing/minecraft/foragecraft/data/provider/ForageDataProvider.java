package me.jonathing.minecraft.foragecraft.data.provider;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import me.jonathing.minecraft.foragecraft.ForageInfo;
import me.jonathing.minecraft.foragecraft.data.objects.IToJson;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.HashCache;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import static me.jonathing.minecraft.foragecraft.data.ForageCraftData.LOGGER;

public abstract class ForageDataProvider<D extends IToJson<D>> implements DataProvider
{
    protected static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    protected Map<ResourceLocation, D> data;
    protected final DataGenerator generator;
    protected final String name;

    public ForageDataProvider(DataGenerator generator, String name)
    {
        this.generator = generator;
        this.name = ForageInfo.MOD_ID + "/" + name;
    }

    @Override
    public void run(@Nonnull HashCache cache)
    {
        this.data = this.gatherData();

        Path outputFolder = this.generator.getOutputFolder();

        this.data.forEach((key, value) ->
        {
            Path outputFile = this.createPath(outputFolder, key);

            try
            {
                JsonObject json = value.toJson();
                DataProvider.save(GSON, cache, json, outputFile);
            }
            catch (IOException e)
            {
                LOGGER.error(String.format("Couldn't save %s %s", this.name, outputFile), e);
            }
        });
    }

    protected abstract Map<ResourceLocation, D> gatherData();

    protected Path createPath(Path outputFolder, ResourceLocation dataEntryName)
    {
        return outputFolder.resolve("data/" + dataEntryName.getNamespace() + "/" + this.name + "/" + dataEntryName.getPath() + ".json");
    }

    @Override
    @Nonnull
    public abstract String getName();
}
