// class imports
var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var InsnNode = Java.type('org.objectweb.asm.tree.InsnNode');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode');
var FieldInsnNode = Java.type('org.objectweb.asm.tree.FieldInsnNode');
var JumpInsnNode = Java.type('org.objectweb.asm.tree.JumpInsnNode');
var LabelNode = Java.type('org.objectweb.asm.tree.LabelNode');

var ASMAPI = Java.type('net.minecraftforge.coremod.api.ASMAPI');

/**
 * The Item coremod hooks into the Item class and injects the ASM bytecode presented in the transformers returned by
 * this method.
 *
 * @returns {{useOn: {transformer: (function(*): *), target: {methodDesc: string, methodName: string, type: string, class: string}}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'useOn': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.world.item.Item',
                'methodName': 'm_6225_',
                'methodDesc': '(Lnet/minecraft/world/item/context/UseOnContext;)Lnet/minecraft/world/InteractionResult;'
            },
            'transformer': useOn
        }
    }
}

/**
 * This function transforms the useOn() method to account for the ForageCraft stick's functionality. For more
 * information on how this is done, read the javadoc documentation in the ItemCoreModHelper java class located in
 * me.jonathing.minecraft.foragecraft.coremod.hooks.coremod.
 *
 * This is the following Java code that is injected into the head of the method in bytecode (mappings included):
 * <pre>
 *     if (p_195939_1_.getItemInHand().getItem().equals(Items.STICK))
 *         return me.jonathing.minecraft.foragecraft.coremod.hooks.ItemHooks.useOn(pContext);
 * </pre>
 *
 * @param method The useOn() method's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed method.
 */
function useOn(method) {
    // get method instructions
    var instructions = method.instructions;
    var insn = instructions.get(0);

    // new label for else clause
    var label = new LabelNode();

    // shove our instructions into the head of the method
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ALOAD, 1));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKEVIRTUAL, 'net/minecraft/world/item/context/UseOnContext', ASMAPI.mapMethod('m_43722_'), '()Lnet/minecraft/world/item/ItemStack;'));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKEVIRTUAL, 'net/minecraft/world/item/ItemStack', ASMAPI.mapMethod('m_41720_'), '()Lnet/minecraft/world/item/Item;'));
    instructions.insertBefore(insn, new FieldInsnNode(Opcodes.GETSTATIC, 'net/minecraft/world/item/Items', ASMAPI.mapField('f_42398_'), 'Lnet/minecraft/world/item/Item;'));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKEVIRTUAL, 'java/lang/Object', 'equals', '(Ljava/lang/Object;)Z'));
    instructions.insertBefore(insn, new JumpInsnNode(Opcodes.IFEQ, label));
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ALOAD, 1));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKESTATIC, 'me/jonathing/minecraft/foragecraft/coremod/hooks/ItemHooks', 'useOn', '(Lnet/minecraft/world/item/context/UseOnContext;)Lnet/minecraft/world/InteractionResult;'));
    instructions.insertBefore(insn, new InsnNode(Opcodes.ARETURN));
    instructions.insertBefore(insn, label);

    // finish up
    return method;
}
