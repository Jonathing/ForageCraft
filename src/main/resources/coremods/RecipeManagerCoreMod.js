// class imports
var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode');

/**
 * The RecipeManager coremod hooks into the RecipeManager class and injects the ASM bytecode presented in the
 * transformers returned by this method.
 *
 * @returns {{apply: {transformer: (function(*): *), target: {methodDesc: string, methodName: string, type: string, class: string}}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'apply': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.world.item.crafting.RecipeManager',
                'methodName': 'm_5787_',
                'methodDesc': '(Ljava/lang/Object;Lnet/minecraft/server/packs/resources/ResourceManager;Lnet/minecraft/util/profiling/ProfilerFiller;)V'
            },
            'transformer': apply
        }
    }
}

/**
 * This function transforms the apply() method to give the map containing all of the recipes to the
 * RecipeManagerCoreModHelper java class for optional recipe filtering.
 *
 * This is the following Java code that is injected into the head of the method in bytecode (mappings included):
 * <pre>
 *     me.jonathing.minecraft.foragecraft.coremod.hooks.RecipeManagerHooks.removeOptionalRecipes(pObject);
 * </pre>
 *
 * @param method The apply() method's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed method.
 */
function apply(method) {
    // get method instructions
    var instructions = method.instructions;
    var insn = instructions.get(0);

    // shove our instructions into the head of the method
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ALOAD, 1));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKESTATIC, 'me/jonathing/minecraft/foragecraft/coremod/hooks/RecipeManagerHooks', 'removeOptionalRecipes', '(Ljava/util/Map;)V'));

    // finish up
    return method;
}
