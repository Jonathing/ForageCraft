// class imports
var Opcodes = Java.type('org.objectweb.asm.Opcodes');
var VarInsnNode = Java.type('org.objectweb.asm.tree.VarInsnNode');
var MethodInsnNode = Java.type('org.objectweb.asm.tree.MethodInsnNode');
var JumpInsnNode = Java.type('org.objectweb.asm.tree.JumpInsnNode');
var LabelNode = Java.type('org.objectweb.asm.tree.LabelNode');
var TypeInsnNode = Java.type('org.objectweb.asm.tree.TypeInsnNode');

/**
 * The Entity coremod hooks into the Entity class and injects the ASM bytecode presented in the transformers returned by
 * this method.
 *
 * @returns {{spawnAtLocation: {transformer: (function(*): *), target: {methodDesc: string, methodName: string, type: string, class: string}}}} The transformers to use for this coremod.
 */
function initializeCoreMod() {
    return {
        'spawnAtLocation': {
            'target': {
                'type': 'METHOD',
                'class': 'net.minecraft.world.entity.Entity',
                'methodName': 'm_19998_',
                'methodDesc': '(Lnet/minecraft/world/level/ItemLike;)Lnet/minecraft/world/entity/item/ItemEntity;'
            },
            'transformer': spawnAtLocation
        }
    }
}

/**
 * This function transforms the spawnAtLocation() method to ensure that the game never attempts to spawn a
 * DecorativeBlock's block item and to rather get the designated representative item given by
 * DecorativeBlock.getDecorativeItem().
 *
 * This is the following Java code that is injected into the head of the method in bytecode (mappings included):
 * <pre>
 *     if (pItem instanceof me.jonathing.minecraft.foragecraft.common.block.template.DecorativeBlock)
 *         pItem = ((me.jonathing.minecraft.foragecraft.common.block.template.DecorativeBlock) pItem).getDecorativeItem();
 * </pre>
 *
 * @param method The spawnAtLocation() method's bytecode that will be transformed by this coremod.
 * @returns {*} The transformed method.
 */
function spawnAtLocation(method) {
    // get method instructions
    var instructions = method.instructions;
    var insn = instructions.get(0);

    // new label for else clause
    var label = new LabelNode();

    // shove our instructions into the head of the method
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ALOAD, 1));
    instructions.insertBefore(insn, new TypeInsnNode(Opcodes.INSTANCEOF, 'me/jonathing/minecraft/foragecraft/common/block/template/DecorativeBlock'));
    instructions.insertBefore(insn, new JumpInsnNode(Opcodes.IFEQ, label));
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ALOAD, 1));
    instructions.insertBefore(insn, new TypeInsnNode(Opcodes.CHECKCAST, 'me/jonathing/minecraft/foragecraft/common/block/template/DecorativeBlock'));
    instructions.insertBefore(insn, new MethodInsnNode(Opcodes.INVOKEVIRTUAL, 'me/jonathing/minecraft/foragecraft/common/block/template/DecorativeBlock', 'getDecorativeItem', '()Lnet/minecraft/world/item/Item;'));
    instructions.insertBefore(insn, new VarInsnNode(Opcodes.ASTORE, 1));
    instructions.insertBefore(insn, label);

    // finish up
    return method;
}
